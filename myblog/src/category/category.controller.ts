import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { Category } from './category.entity/category.entity';
import { CategoryService } from './category.service';

@Controller('category')
export class CategoryController {
    constructor(
        private readonly categoryService: CategoryService
    ) { }

    @Get()
    get(): Promise<Category[]> {
        return this.categoryService.findAll();
    }

    @Post()
    create(@Body() category: Category) {
        if (category.name === null) {
            return 'Chưa nhập tên danh mục';
        } else {
            return this.categoryService.create(category);
        }     
    }

    @Put()
    update(@Body() category: Category) {
        if (category.name === null) {
            return 'Chưa nhập tên danh mục';
        } else {
            return this.categoryService.update(category);
        }         
    }

    @Delete(':id')
    delete(@Param() params) {
        return this.categoryService.delete(params.id);
    }
}
