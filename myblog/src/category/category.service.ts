import { Injectable } from '@nestjs/common';
import { Category } from './category.entity/category.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(Category)
        private readonly categoryRepo: Repository<Category>
    ) { }

    async findAll(): Promise<Category[]> {
        return await this.categoryRepo.find();
    }

    async create(Category: Category): Promise<Category> {
        return await this.categoryRepo.save(Category);
    }

    async update(Category: Category): Promise<UpdateResult> {
        return await this.categoryRepo.update(Category.id, Category);
    }

    async delete(id): Promise<DeleteResult> {
        return await this.categoryRepo.delete(id);
    }
}

