import { Injectable } from '@nestjs/common';
import { Blog } from './blog.entity/blog.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class BlogService {
    constructor(
        @InjectRepository(Blog)
        private readonly blogRepo: Repository<Blog>
    ) { }

    async findAll(): Promise<Blog[]> {
        return await this.blogRepo.find();
    }

    async detail(id: number): Promise<Blog> {
        return await this.blogRepo.findOneBy({ id: id });
    }

    async create(blog: Blog): Promise<Blog> {
        return await this.blogRepo.save(blog);
    }

    async update(blog: Blog): Promise<UpdateResult> {
        return await this.blogRepo.update(blog.id, blog);
    }

    async delete(id): Promise<DeleteResult> {
        return await this.blogRepo.delete(id);
    }

    async search(tittle: string): Promise<Blog[]> {
        return await this.blogRepo.findBy({ tittle: tittle });
    }
}
