import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Category } from 'src/category/category.entity/category.entity';

@Entity()
export class Blog {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 500 })
    tittle: string;

    @Column('text')
    description: string;

    @Column({ length: 500 })
    author: string;

    @ManyToOne(() => Category, (category) => category.blog)
    category: Category;
}