import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { Blog } from './blog.entity/blog.entity';
import { BlogService } from './blog.service';

@Controller('blog')
export class BlogController {
    constructor(
        private readonly blogService: BlogService
    ) { }

    @Get()
    get(@Param() blog: Blog): Promise<Blog[]> {
        if (blog.tittle == null) {
            return this.blogService.findAll();
        } else {
            return this.blogService.search(blog.tittle);
        }
    }

    @Get(':id')
    detail(@Param() param) {
        return this.blogService.detail(param.id);
    }

    @Post()
    create(@Body() blog: Blog) {
        if (blog.tittle == null) {
            return 'Chưa nhập tên blog';
        } else {
            return this.blogService.create(blog);
        }          
    }

    @Put()
    update(@Body() blog: Blog) {
        if (blog.tittle == null) {
            return 'Chưa nhập tên blog';
        } else {
            return this.blogService.update(blog);
        }       
    }

    @Delete(':id')
    delete(@Param() params) {
        return this.blogService.delete(params.id);
    }
}

